import Firebase from 'firebase/app';
import 'firebase/database';

let config = {
    apiKey: "AIzaSyA1PAsB3AjvBYFQzLcGRM5B3sY9xAboLHM",
    authDomain: "coding-challenge-b4243.firebaseapp.com",
    databaseURL: "https://coding-challenge-b4243.firebaseio.com",
    projectId: "coding-challenge-b4243",
    storageBucket: "coding-challenge-b4243.appspot.com",
    messagingSenderId: "201054380714",
    appId: "1:201054380714:web:1d22307b35ced30084a6d3",
    measurementId: "G-QRMGSRPXEE"
};

const app = Firebase.initializeApp(config);

export const db = app.database();